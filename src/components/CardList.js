//
// Dmitry Alexandrov
// B.RF Group
//
import React, { useState } from "react"
import ListItem from "./ListItem"

export default function CardList() {
  const [text, setText] = useState("")
  const [cards, setCards] = useState([])
  const [card, setCard] = useState()

  function handleInput(e) { // edit text
    setText(e.target.value) // save entered text
  }

  function handleCancel() { // cancel text input    
    setCard() // clear saved card
    setText("") // clear text
  }

  function handleAdd(e) { // add or change card
    e.preventDefault()
    if (card) { // change card
      if (text) { // not empty text
        const editedCard = { ...card, text: text }
        // or (the same):
        // const editedCard = {
        //   id: card.id,
        //   text: text
        // }

        if (editedCard.text !== card.text) { // reorder cards
          setCards([editedCard, ...cards.filter((c) => c.id !== card.id)])
        }
        setText("") // clear text
      } else { // remove empty card
        removeHandle(card.id)
      }
      setCard() // clear saved card - not changing
    } else { // add card
      if (text) { // not empty text
        const newCard = {
          id: "_" + Math.random().toString(36).substring(2, 9),
          text: text
        }
        setCards([newCard, ...cards]) // save new card on top
        setText("") // clear text
      }
    }
  }

  function removeHandle(id) { // remove the card
    setCards(cards.filter((c) => c.id !== id))
    handleCancel()
  }

  function editHandle(id) { // edit the card
    const card = cards.filter((c) => c.id === id)[0]
    setCard(card) // save the card for changing
    setText(card.text) // place text from the card to change
  }

  return (
    <div className="app">
      <form className="add-card"> 
        <label class="">
          { card ? <p>Правка текста карточки</p> :
                   <p>Введите текст для добавления карточки</p>}

          <textarea value={text} onInput={handleInput} rows="5" />
        </label>
        <div className="buttons">
          {((text && !card) || (card && text !== card.text)) &&
          <button class="button" type="submit"
          onClick={handleAdd}>{card ? (text.length === 0 ?
          "Удалить карточку" : "Сохранить изменения") :
          "Добавить карточку"}</button>}
          
          {((text && !card) || (card && text !== card.text)) &&
          <button class="button" type="cancel"
          onClick={handleCancel}>Отмена</button>}
        </div>
      </form>

      <div className="card-list">
        {cards.map((c) => (
          <ListItem key={c.id} card={c}
                    selected={c === card ? true : false}
                    onRemove={removeHandle} onEdit={editHandle}/>
        ))}
      </div>
    </div>
  )
}
