//
// Dmitry Alexandrov
// B.RF Group
//
import React from "react"

export default function ListItem({ card, selected, onRemove, onEdit, onSelect }) {
  let cname = "card-item"
  if (selected === true) {
    cname = "card-selected-item"
  }
  
  return (
    <div className={cname}>
      <p>{card.text}</p>

      {(cname !== "card-selected-item") &&
      <button className="card-item-edit" type="button"
              onClick={() => { onEdit(card.id) }}>Изменить</button>}
      
      {(cname === "card-selected-item") &&
      <button className="card-item-delete" type="button"
              onClick={() => { onRemove(card.id) }}>Удалить</button>}
    </div>
  )
}
